const User = require('../../models/mongo/user-model')
const User_model = require("../../models/user-model")
const finalResponse = require('../../shared/final-response')
const Cryptr = require('cryptr');
cryptr = new Cryptr('myTotalySecretKey');
const tokenHandler = require('../../config/jwt-config');

module.exports.createUsers = async(req,res)=> {
  
    let validate = User_model.validateUser(req.body,'all')
    let data = {}
    if(validate == true)
    {
         if(req.body.password){req.body.password= await cryptr.encrypt(req.body.password);}
         var user = new User(req.body)
         user.save()
         .then(async user=> {
               if(user._doc.organization && user._doc.organization.length==0){
                 delete user._doc.organization
               }
               if(user._doc.products && user._doc.products.length==0){
                delete user._doc.products
              }
              user._doc._token = await tokenHandler.signToken(user._doc)
              delete user._doc.password
              delete user._doc.__v
              delete user._doc.createdAt
              delete user._doc.updatedAt
             data.response_data = user
             data.message = 'User Created Successfully..'
             finalResponse(res,data,'success')}
           )
           .catch((err)=>{
             if(err.code == 11000) data.message = 'Duplicate entry for email given...'
             else data.message = err
             finalResponse(res,data,'failure')
             
           })
     }
    else
    {
        
         data.message = validate
          finalResponse(res,data,'badrequest')
    }
    
  }

  module.exports.deleteUser = async (req,res)=>{
    let validate = User_model.validateUser(req.query,'id')
    let data = {}
    if(validate == true){
       var user = await User.findById(req.query.id);
       if(!user){
        data.message = 'User with this id not found'
        finalResponse(res,data,'failure')
       }
       else{
           user.status = '1'
           await user.save()
           data.message = 'User Deleted Successfully..'
           finalResponse(res,data,'success')
       }
    }
    else{
          data.message = validate
          finalResponse(res,data,'badrequest')
    }
   
  }

  module.exports.getUsers = (req,res)=>{
    User.find({}, 'name email mobile  products')
    .populate("products")
    .then(data => {
     // let r_data = 
     let rdata = {}
     if(data.length<=0){
       rdata.message = 'No data Found'
      
     }
     else{
        rdata.response_data = data

     }
     finalResponse(res,rdata,'success')
      
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
  }
  module.exports.getUserbyId = (req,res)=>{
    User.findById(req.query.id)
    .populate("products")
    .then(data => {
     // let r_data = 
     let rdata = {}
     if(data.length<=0){
       rdata.message = 'No data Found'
      
     }
     else{
       delete data._doc.password
       delete data._doc.__v
       delete data._doc.createdAt
       delete data._doc.updatedAt
        rdata.response_data = data

     }
     finalResponse(res,rdata,'success')
      
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
  }

  module.exports.updateUser = async (req,res)=>{
    let validate = User_model.validateUser(req.body,'id')
    let data = {}
    if(validate == true){
       var user = await User.findById(req.body.id);
       if(!user){
        data.message = 'User with this id not found'
        finalResponse(res,data,'failure')
       }
       else{
           user.set(req.body)
           var result = await user.save()
           delete result._doc.password
           data.response_data = result
           data.message = 'User Updated Successfully..'
           finalResponse(res,data,'success')
       }
    }
    else{
          data.message = validate
          finalResponse(res,data,'badrequest')
    }
   
  }

 