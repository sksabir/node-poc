const Tutorial = require('../../models/mongo/tutorial')
const Comment = require('../../models/mongo/comment')
const createTutorial = function(tutorial) {
    return Tutorial.create(tutorial).then(docTutorial => {
      console.log("\n>> Created Tutorial:\n", docTutorial);
      return docTutorial;
    });
  };
  
  const createImage = function(tutorialId, image) {
    console.log("\n>> Add Image:\n", image);
    return Tutorial.findByIdAndUpdate(
      tutorialId,
      {
        $push: {
          images: {
            url: image.url,
            caption: image.caption
          }
        }
      },
      { new: true, useFindAndModify: false }
    );
  };
  const createComment = function(tutorialId, comment) {
    return Comment.create(comment).then(docComment => {
      console.log("\n>> Created Comment:\n", docComment);
  
      return Tutorial.findByIdAndUpdate(
        tutorialId,
        { $push: { comments: docComment._id } },
        { new: true, useFindAndModify: false }
      );
    });
  };
  
  const run = async function() {
    var tutorial = await createTutorial({
      title: "Tutorial #1",
      author: "bezkoder"
    });
  
    tutorial = await createImage(tutorial._id, {
      path: "sites/uploads/images/mongodb.png",
      url: "/images/mongodb.png",
      caption: "MongoDB Database",
      createdAt: Date.now()
    });
    console.log("\n>> Tutorial:\n", tutorial);
  
    tutorial = await createImage(tutorial._id, {
      path: "sites/uploads/images/one-to-many.png",
      url: "/images/one-to-many.png",
      caption: "One to Many Relationship",
      createdAt: Date.now()
    });
    console.log("\n>> Tutorial:\n", tutorial);
    tutorial = await createComment(tutorial._id, {
        username: "jack",
        text: "This is a great tutorial.",
        createdAt: Date.now()
      });
      console.log("\n>> Tutorial:\n", tutorial);
    
      tutorial = await createComment(tutorial._id, {
        username: "mary",
        text: "Thank you, it helps me alot.",
        createdAt: Date.now()
      });
      console.log("\n>> Tutorial:\n", tutorial);
  };
  module.exports = run