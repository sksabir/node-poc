const db = require("../models"); // models path depend on your structure
const User = db.User;
const UserModel = require("../models/user-model")
const finalResponse = require('../shared/final-response')
const Cryptr = require('cryptr');
cryptr = new Cryptr('myTotalySecretKey');
const tokenHandler = require('../config/jwt-config');

module.exports.createUsers = async(req,res)=> {
       let validate = UserModel.validateUser(req.body,'all')
       let data = {}
       if(validate == true)
       {
            if(req.file) req.body.image = req.file.filename
            if(req.body.password){req.body.password= await cryptr.encrypt(req.body.password);}
            User.create(req.body)
            .then(async user=> {
                 delete user.dataValues.password
                 delete user.dataValues.updatedAt
                 delete user.dataValues.createdAt
                 user.dataValues._token = await tokenHandler.signToken(user.dataValues)

                data.response_data = user
                data.message = 'User Created Successfully..'
                finalResponse(res,data,'success')}
              )
              .catch((err)=>{
                if(err.original.sqlMessage) data.message = err.original.sqlMessage
                else data.message = err
                finalResponse(res,data,'failure')
                
              })
        }
       else
       {
           
            data.message = validate
             finalResponse(res,data,'badrequest')
       }
       
     }
module.exports.loginUser = (req,res)=>{
  let data = {}
  User.findOne({
    where: {
      email: req.body.email
    }
  }).then(async user =>{
    if(user==null)
    {
      data.message = `${req.body.email} is not present`
      finalResponse(res,data,'success')
    }
    else
    {
      if(user.dataValues.password == null)
      {

        data.message = `Can't login as social login user`;
        finalResponse(res,data,'success')
      }
      else
      {
        let decryptedString = await cryptr.decrypt(user.dataValues.password);
        if(decryptedString == req.body.password)
        {
          delete user.dataValues.password
          delete user.dataValues.updatedAt
          delete user.dataValues.createdAt
          user.dataValues._token = await tokenHandler.signToken(user.dataValues)

         data.response_data = user
         data.message = 'User Login Successfully..'
         finalResponse(res,data,'success')
        }
        else{
           data.message=`Password incorrect`
           finalResponse(res,data,'success')
        }
      }
      
    }
   
         
  })
  .catch((err)=>{
    data.message = err
    finalResponse(res,data,'failure')
  })
}
module.exports.deleteUser = (req,res)=>{
      let validate = UserModel.validateUser(req.query,'id')
      let data = {}
      if(validate == true){
       
        req.query.status = '1'
      User.update(req.query, {
        where: { id: req.query.id }
      })
        .then(num => {
         
          if (num[0] == 1) {
            data.message= `User deleted successfully`
            finalResponse(res,data,'success')
          } else {
            
              data.message= `Cannot delete User with id=${req.query.id}. Maybe User was not found or req.body is empty!`
              finalResponse(res,data,'failure')
          }
        })
        .catch(err => {
         
          data.message = err
          finalResponse(res,data,'failure')
        });
      }
      else{
            data.message = validate
            finalResponse(res,data,'badrequest')
      }
     
    }
        module.exports.getUsers = (req,res)=>{
        User.findAll({attributes: ['id','name', 'mobile','email']})
        .then(data => {
         // let r_data = 
         let rdata = {}
         if(data.length<=0){
           rdata.message = 'No data Found'
          
         }
         else{
            rdata.response_data = data

         }
         finalResponse(res,rdata,'success')
          
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving User."
          });
        });
      }
      module.exports.updateUser = (req,res)=>{
        let uid = req.body.id
        let validate = UserModel.validateUser(req.body,'id')
        let data = {}
        if(validate == true){
        delete req.body.id
          User.update(req.body,{where:{id:uid}})
          .then(num => {
           
            if (num == 1) {
              data.message= `User updated successfully`
              finalResponse(res,data,'success')
            } else {
              
                data.message= `Cannot update User with id=${uid}. Maybe User was not found or req.body is empty!`
                finalResponse(res,data,'failure')
            }
          })
          .catch(err => {
          
            data.message = err
            finalResponse(res,data,'failure')
          });
      }
      else{
        data.message = validate
        finalResponse(res,data,'badrequest')
      }
    }

