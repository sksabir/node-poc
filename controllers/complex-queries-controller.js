const db = require("../models"); 
const User = db.User;
const Product = db.Product;
const sequelize = db.sequelize;
const pool = require('../config/db-config');
const finalResponse = require('../shared/final-response')

module.exports.userWithProducts = (req,res)=>{
    User.findAll({
      attributes: ['id','name','email'],
      include: [{
        model: Product,
        attributes: ['id','user_id','product_name'],
        required: false
       }]
    }).then(posts => {
      let data = {}
      data.response_data = {}
      data.response_data['users'] = posts
      finalResponse(res,data,'success')
    })
    .catch((err)=>{
       let data = {}
       data.message = err
       finalResponse(res,data,'failure')
    })
  }
  module.exports.userWithProductsAndOrg = (req,res)=>{
    User.findAll({
      where: {
        id: req.query.id
       },
      attributes: ['id','name','email',[sequelize.literal(`(SELECT org_name FROM organizations WHERE organizations.user_id = ${req.query.id})`), 'org_name']]
        ,
      include: [{
        model: Product,
        attributes: ['id','user_id','product_name'],
        required: false
       }
      ]
    }).then(posts => {
      let data = {};
     
      if(posts[0]._previousDataValues) data.response_data = posts[0]._previousDataValues
      else data.response_data = posts
     
      finalResponse(res,data,'success')
    })
    .catch((err)=>{
       let data = {}
       data.message = err
       finalResponse(res,data,'failure')
    })
  }



  module.exports.userWithProductsraw = (req,res)=>{
    let sql = `SELECT users.id , users.name ,users.email, products.user_id, products.user_id, products.id as p_id ,products.product_name FROM users LEFT JOIN products on users.id = products.user_id`;
    pool.query(sql ,function (error, response){
        let data ={}
        if(error){
            data.message = error
            finalResponse(res,data,'failure')
        }
        else{
          
            let response_data = []
            for(let user of response){
                if(response_data.users==undefined)
                {
                  response_data['users'] = []
                    response_data['users'].push(
                        {
                            id:user.id,
                            name:user.name,
                            email:user.email,
                            products:[{
                              id:user.p_id,
                              product_name:user.product_name
                            }]
                        })
                }
                else
                {
                  let is_present = response_data.users.findIndex(x => x.id ===user.id);
                if(is_present>-1){
                  response_data.users[is_present].products.push( {id:user.p_id,
                    product_name:user.product_name
                  })
                 
                }
                else{
                  response_data.users.push(
                    {
                        id:user.id,
                        name:user.name,
                        email:user.email,
                        products:[{
                          id:user.p_id,
                          product_name:user.product_name
                        }]
                    })
                }
                }
               
            }
            
            data.response_data = response_data.users


            finalResponse(res,data,'success')
        }
    })

  }
  module.exports.userWithProductsAndOrgRaw = (req,res)=>{
    let sql = `SELECT user.id, user.name, user.email, (SELECT org_name FROM organizations WHERE organizations.user_id = ${req.query.id}) AS org_name, products.id as p_id, products.user_id, products.product_name FROM users AS user LEFT OUTER JOIN products ON user.id = products.user_id WHERE user.id = ${req.query.id}`;
    pool.query(sql ,function (error, response){
        let data ={}
        if(error){
            data.message = error
            finalResponse(res,data,'failure')
        }
        else{
            if(response.length>0){
              let response_data = []
              for(let user of response){
                  if(response_data.users==undefined)
                  {
                    response_data['users'] = []
                      response_data['users'].push(
                          {
                              id:user.id,
                              name:user.name,
                              email:user.email,
                              organization: user.org_name,
                              products:[{
                                id:user.p_id,
                                product_name:user.product_name
                              }]
                          })
                  }
                  else
                  {
                    let is_present = response_data.users.findIndex(x => x.id ===user.user_id);
                  if(is_present>-1){
                    response_data.users[is_present].products.push( {id:user.p_id,
                      product_name:user.product_name
                    })
                   
                  }
                  else{
                    response_data.users.push(
                      {
                          id:user.id,
                          name:user.name,
                          email:user.email,
                          products:[{
                            id:user.p_id,
                            product_name:user.product_name
                          }]
                      })
                  }
                  }
                 
              }
              
              data.response_data = response_data.users
              finalResponse(res,data,'success')
            }
            else{
              data.message = 'No data found'
              finalResponse(res,data,'failure')
            }
          
        }
    })
}