var multer = require('multer');
const finalResponse = require('../shared/final-response')

module.exports.uploadfile = (req,res,next) => {
    var upload = multer({
        storage: multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, 'public/files')
            },
            filename: function (req, file, cb) {
                const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, file.fieldname + '-' + uniqueSuffix)
            }
        })
    })
    upload.single('profile')(req, res,next)
    let data = {}
    data.message = 'File uploaded Successfully..'
    finalResponse(res,data,'success')
   
}


