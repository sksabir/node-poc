-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2021 at 03:09 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cn_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `org_name` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `user_id`, `org_name`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'CN', '2021-05-31 06:41:51', '2021-05-31 06:41:51'),
(2, 2, 'JIET', '2021-05-31 06:42:08', '2021-05-31 06:42:08'),
(3, 3, 'CMIE', '2021-05-31 06:42:31', '2021-05-31 06:42:31'),
(4, 4, 'Bachpan', '2021-05-31 06:42:51', '2021-05-31 06:42:51'),
(5, 5, 'JEN', '2021-05-31 06:43:06', '2021-05-31 06:43:06');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `product_name`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Mobile', '2021-05-31 06:11:42', '2021-05-31 06:11:42'),
(2, 1, 'Televison', '2021-05-31 06:11:42', '2021-05-31 06:11:42'),
(3, 2, 'Televison', '2021-05-31 06:11:42', '2021-05-31 06:11:42'),
(4, 3, 'Mobile', '2021-05-31 06:11:42', '2021-05-31 06:11:42'),
(5, 4, 'Washing Machine', '2021-05-31 06:11:42', '2021-05-31 06:11:42'),
(6, 4, 'Mobile', '2021-05-31 06:11:42', '2021-05-31 06:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `isDeleted` enum('0','1') DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `isDeleted`, `password`, `image`, `createdAt`, `updatedAt`) VALUES
(1, 'Surbhi', 'surbhiharsh09@gmail.com', '8561014743', '0', NULL, NULL, '2021-05-31 05:58:12', '2021-05-31 05:58:12'),
(2, 'Shruti', 'shrutiharsh68@gmail.com', '9672987400', '1', NULL, NULL, '2021-05-31 05:58:12', '2021-06-22 17:28:40'),
(3, 'Vaishali', 'vaishaliharsh@gmail.com', '9672987400', '0', NULL, NULL, '2021-05-31 05:58:12', '2021-05-31 05:58:12'),
(4, 'Shivansh', 'harshshivansh@gmail.com', '9672987400', '0', NULL, NULL, '2021-05-31 05:58:12', '2021-05-31 05:58:12'),
(5, 'Viru', 'virujat@gmail.com', '9782112696', '0', NULL, NULL, '2021-05-31 05:58:12', '2021-05-31 05:58:12'),
(6, 'surbhiharsh', 'surbhiharsh@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-04 13:14:14', '2021-06-04 13:14:14'),
(15, 'surbhiharsh', 'surbhiharsh25@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 05:57:21', '2021-06-15 05:57:21'),
(17, 'surbhiharsh', 'surbhiharsh225@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 05:58:20', '2021-06-15 05:58:20'),
(18, 'shruti harsh', 'shrutiharsh@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 05:59:16', '2021-06-15 05:59:16'),
(20, 'shruti harsh', 'shrutiharsh19@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 06:01:47', '2021-06-15 06:01:47'),
(21, 'shruti harsh', 'shrutiharsh25@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 06:02:35', '2021-06-15 06:02:35'),
(22, 'shruti harsh', 'shrutiharsh251@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 06:03:08', '2021-06-15 06:03:08'),
(23, 'shruti harsh', 'shrutiharsh254@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-15 06:05:48', '2021-06-15 06:05:48'),
(28, 'surbhiharsh', 'surbhiharsh1@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-22 13:18:30', '2021-06-22 13:18:30'),
(29, 'surbhiharsh', 'surbhiharsh11@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-22 13:19:43', '2021-06-22 13:19:43'),
(30, 'surbhiharsh', 'surbhiharsh21@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-22 13:20:12', '2021-06-22 13:20:12'),
(32, 'surbhiharsh', 'surbhiharsh251@gmail.com', '42542554325', '0', '51dc0775fe6be4821fecba8c0d3af98e4bfbe4688b92e28676ad51386cf93c180ed39b4d33bd27886f3608ab5d78ea0420eaa96128e4ca5b67ff2c92d631aa83af0b7843d6fc27a7d16c4f72bc19aad7dbad667ee58af5f5b7a81f33d4af67f926f673cf', NULL, '2021-06-22 13:21:36', '2021-06-22 13:21:36'),
(34, 'surbhiharsh', 'surbhiharsh255@gmail.com', '42542554325', '0', 'a2b5668b7d5e7760579e3ef0f8f8f6d5924874afdcc4073a1d706da5543ebd1036a38572fa28e3e6cbdb71eeb9a5ad733b350c81fed030b787b961667deace8cea583f0083adabaecee55fba74be48e541272063e0fd9123ba68890adccf369114097a45', NULL, '2021-06-22 13:22:41', '2021-06-22 13:22:41'),
(38, 'surbhiharsh', 'surbhiharsh210@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-23 04:21:04', '2021-06-23 04:21:04'),
(39, 'surbhiharsh', 'surbhiharsh2100@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-23 04:22:03', '2021-06-23 04:22:03'),
(40, 'surbhiharsh', 'surbhiharsh21000@gmail.com', '42542554325', '0', NULL, NULL, '2021-06-23 04:23:48', '2021-06-23 04:23:48'),
(42, 'surbhiharsh', 'surbhiharshgmail.com', '42542554325', '0', NULL, NULL, '2021-06-23 08:25:24', '2021-06-23 08:25:24'),
(44, 'surbhiharsh', 'surbhi@gmail.com', '42542554325', '0', 'f1152442c6c91cd2b19eadf414d5665955f88a0cae13ae26a8867228bdbbab0c793062f4a0ef4308ddec782623d90d77197307c321a19f1014c3991cd2534e9bfbc3832ce054c73e149ebfaccebc409a758febb78f59f23e3568f388ca2c7e8402ea5a1a9f169c83', NULL, '2021-06-23 08:26:05', '2021-06-23 08:26:05'),
(45, 'surbhiharsh', 'surbhii@gmail.com', '42542554325', '0', 'da62e489cb1b3a6be414872a9e492cbc7768435dcf44115648ac395423d8e293c5b43e32742d2790f8572a0d9800c72b35b94a6f99d2426555bfa625ccece4c12bdf1521cd0ca619e0e41a3cbef73d6f3dd8352be9d02f1a3ebb2702bbb1903ffa4d8560afaba19d', NULL, '2021-06-23 08:27:24', '2021-06-23 08:27:24'),
(47, 'surbhiharsh', 'surbhii3@gmail.com', '982484825', '0', 'd7432c351d0d06781465f0c4dd6c411a8e391a83641a7eff8c7ba957baf920471965e341a5f3ac62cd2c67a194de68cf08444bb94d029ae4b395f53941921d5eab3900d05e28c46723c05ec8190aa8c0f76fa4f0eab081aa8769dc1f62a2fa1b0d446bdc3ea2d42f', '715117bd724f6ef0a2bda66972c53f6a', '2021-06-23 09:40:36', '2021-06-23 09:40:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
