const usercontroller = require('../controllers/user-controller');
const musercontroller = require('../controllers/mongo/user-controller');
const upload = require('../controllers/file-uploadation-controller');
const complexcontroller = require('../controllers/complex-queries-controller');
const finalResponse = require('../shared/final-response');
const  config  = require('config');
var multer  = require('multer')
var upload1 = multer({ dest: 'public/files/' })
 require('./swagger')
module.exports =  async(app,req,res)=>  {
    
    //Routes for sequelize api
    if(config.get('DATABASE_TYPE')=='sequelize'){
        app.post('/api/create-user', upload1.single('image'),usercontroller.createUsers);
        app.post('/api/login',usercontroller.loginUser);
        app.delete('/api/delete-user', usercontroller.deleteUser);
        app.get('/api/all-users', usercontroller.getUsers);
        app.put('/api/update-user', usercontroller.updateUser);
           /* Routes to implement the complex queries 
       1) approach using sequlize: routes used are ==> */
        app.get('/api/get-users',complexcontroller.userWithProducts)
        app.get('/api/get-user',complexcontroller.userWithProductsAndOrg)
        /* 2) approach using raw: routes used are ==> */
        app.get('/api/raw-get-users',complexcontroller.userWithProductsraw)
        app.get('/api/raw-get-user',complexcontroller.userWithProductsAndOrgRaw)
        //app.use('/api/uploadfile',upload.uploadfile)

    }
    else if(config.get('DATABASE_TYPE')=='mongo') {
         //Routes for mongodb api
        app.post('/api/create-Users',musercontroller.createUsers);
        app.delete('/api/mdelete-User',musercontroller.deleteUser)
        app.get('/api/getall-users',musercontroller.getUsers)
        app.put('/api/update-Users',musercontroller.updateUser)
        app.use('/api/user-by-id',musercontroller.getUserbyId)
    }
    else
    {
        let data = {}
        data.message `You are connected to ${config.get('DATABASE_TYPE')} database and trying other db operation that can't be performed`
        finalResponse(res,data,'failure')
    }
    
 
};