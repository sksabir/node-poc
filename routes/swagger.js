/**
 * @swagger
 * /api/login:
 *   post:
 *     summary: Login to a user in sequelize 
 *     description: Login to the application
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     requestBody:
 *         required: true
 *         content: 
 *              application/json:
 *                schema:
 *                   email:
 *                     type: string
 *                   password:
 *                     type: string
 *                   example:
 *                     email: surbhi@gmail.com
 *                     password: 12344567
 *     responses:
 *        200:
 *          description: login
 *        400:
 *          description: bad request
 */
/**
 * @swagger
 * /api/create-user:
 *   post:
 *     summary: Create a  user in sequelize.
 *     description: Create a user in application using formdata
 *     requestBody:
 *      content: 
 *        multipart/form-data: # Media type
 *          schema: 
 *            type: object
 *            properties:      # Request parts
 *               name:       # Part2 (object)
 *                 type: string
 *                 required: true
 *               email:
 *                 type: string
 *                 required: true
 *               password: 
 *                 type: string
 *                 required: true
 *               mobile: 
 *                 type: string
 *                 required: true
 *               image:  # Part 3 (an image)
 *                 type: file
 *     responses:
 *       200:
 *         description: Created
 *       400:
 *         description: Bad Request
*/
/**
 * @swagger
 * /api/all-users:
 *   get:
 *     summary: Retrieve all users in sequelize db.
 *     description: Retrieve all users. 
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/get-users:
 *   get:
 *     summary: Retrieve  users in sequelize db with products.
 *     description: Retrieve all users. 
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/get-user:
 *   get:
 *     summary: Retrieve a user in sequelize db with product and organization .
 *     description: Retrieve a user. 
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     parameters:
 *       - in: query
 *         name: id
 *         type: integer
 *         description: The id of the user.
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/raw-get-users:
 *   get:
 *     summary: Retrieve all users using raw query.
 *     description: Retrieve all users. 
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/raw-get-user:
 *   get:
 *     summary: Retrieve a user using raw query.
 *     description: Retrieve all users. 
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/update-user:
 *   put:
 *     summary: Update a user in sequelize.
 *     description: Pass a id of user to update it. 
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     requestBody:
 *         required: true
 *         content: 
 *              application/json:
 *                schema:
 *                   id: 
 *                     type: number 
 *                     required: true
 *                   name: 
 *                     type: string
 *                     required: true
 *                   example:
 *                     id: 1
 *                     name: updated name
 *                     email: youremail
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *       400: 
 *         description: Bad request
 *     security: [{ "authorization": [] }]
*/
/**
 * @swagger
 * /api/delete-user:
 *   delete:
 *     summary: Delete a user in sequelize.
 *     description: Pass a id of user to delete it. 
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     parameters:
 *       - in: query
 *         name: id
 *         type: integer
 *         description: The id of the user to be deleted.
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *       400: 
 *         description: Bad request
 *     security: [{ "authorization": [] }]
 */

/**
 * @swagger
 * /api/create-Users:
 *   post:
 *     summary: Create a user in mongodb.
 *     description: Create user in the application
 *     produces: 
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     requestBody:
 *         required: true
 *         content: 
 *              application/json:
 *                schema:
 *                   name: 
 *                     type: string
 *                     required: true
 *                   mobile:
 *                     type: number
 *                     required: true
 *                   email:
 *                     type: string
 *                     required: true
 *                   password:
 *                     type: string
 *                   example:
 *                     name: yourname
 *                     email: youremail
 *                     mobile: 313424109
 *                     password: yourpassword
 *     responses:
 *       200:
 *         description: Created
 *       400:
 *         description: Bad Request
*/
/**
 * @swagger
 * /api/getall-users:
 *   get:
 *     summary: Retrieve all users in mongodb.
 *     description: Retrieve all users. 
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/update-Users:
 *   put:
 *     summary: Update a user in mongodb.
 *     description: Pass a id of user to update it. 
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     requestBody:
 *         required: true
 *         content: 
 *              application/json:
 *                schema:
 *                   id: 
 *                     type: string 
 *                     required: true
 *                   name: 
 *                     type: string
 *                     required: true
 *                   example:
 *                     id: "60e42ae67f26532e30666a72"
 *                     name: updated name
 *                     email: youremail
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *       400: 
 *         description: Bad request
 *     security: [{ "authorization": [] }]
 */
/**
 * @swagger
 * /api/mdelete-User:
 *   delete:
 *     summary: Delete a user in mongodb.
 *     description: Pass a id of user to delete it. 
 *     produces:
 *       - application/xml
 *       - application/json
 *     consumes:
 *       - application/json
 *       - application/xml
 *     parameters:
 *       - in: query
 *         name: id
 *         type: integer
 *         description: The id of the user to be deleted.
 *     responses:
 *       200:
 *         description: Fetched
 *       401:
 *         description: Invalid Token
 *       400: 
 *         description: Bad request
 *     security: [{ "authorization": [] }]
 */
