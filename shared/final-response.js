function finalResponse(res,data,message) {
    var json_res ={};
    if(data.message){
          json_res.message = data.message
          delete data.message
    }
    if(message == 'success'){ 
       
        json_res.status= true
        if(data.response_data)  json_res.data = data.response_data
        res.status(200).json(json_res);
    }
    else if(message == 'failure'){
 
        json_res.status= false
        if(data.response_data)  json_res.data = data.response_data
        res.status(400).json(json_res);
    }
    else if(message == 'badrequest'){
 
        json_res.status= false
        if(data.response_data)  json_res.data = data.response_data
        res.status(400).json(json_res);
    }
}


 module.exports = finalResponse

