const swaggerJsdoc = require("swagger-jsdoc");
var swaggerUi = require("swagger-ui-express");
const swaggerDefinition = {
    openapi: '3.0.1',
    info: {
     
      title: 'API DOCUMENTATION',
      description: 'User management API',
     
    },
    servers: [
      {
        url: 'http://localhost:3000/',
        description: 'Local server'
      }],
      // security: [
      //   {
      //     authorization: []
      //   }
      // ],
      components: {
        /* ... */
        securitySchemes: {
            authorization: {
                type: 'apiKey',
                name: 'Authorization',
                scheme: 'bearer',
                in: 'header'
          }
        }
      }
    /* ... */
  };
     
  module.exports =  async(app)=>  {    
     const options = {
       swaggerDefinition,
       // Paths to files containing OpenAPI definitions
       apis: ['./routes/*.js'],
     };
     
     const swaggerSpec = swaggerJsdoc(options);
     app.use(
       "/api-docs",
       swaggerUi.serve,
       swaggerUi.setup(swaggerSpec)
     );
  }