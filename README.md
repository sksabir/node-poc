

# Node-poc

This project include all the operations performed in node js with unit testing.

---
## Requirements

For development, you will only need Node.js and a node global package, npm, installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Other Operating Systems


  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v14.16.1

    $ npm --version
    6.14.12
---
### XAMPP
 - ## XAMPP INSTALLATION ON WINDOWS 
Open the XAMPP website. Go to https://www.apachefriends.org/index.html in your computer's web browser. Click xampp for windows and double click the downloaded file finish the installation process.

### MONGODB 
 - ## MONGODB INSTALLATION ON WINDOWS
Opne https://www.mongodb.com/try/download/community on your browser and select platform as windows click the download button. Click the downloaded file and install.

## Install

    $ git clone https://gitlab.com/sksabir/node-poc.git
    $ cd node-poc
    $ npm i

## Configure app
- ### Settings to do: 
- Open `config\default-example.json` , create default.json in config folder and  copy paste code written in env-example file to the new files created then edit it with your settings. You will need:

    $ mysql database setting;
    $ mongodb setting;
    $ mail server settings;

Do the above steps for developemnt and production also.

- For setting enviroment variable to developement to dev or production run set NODE_ENV = dev in windows and export NODE_ENV = dev in mac

- To connect to mongodb set DATABASE_TYPE = "mongo" in the above json files created and for sequelize set DATABASE_TYPE = "sequelize"

- Create public folder in root of the app.

  ## Swagger API Documentation
   The url for Swagger API Documentation is : http://localhost:3000/api-docs/ 

   - ## References Links:
    - https://blog.logrocket.com/documenting-your-express-api-with-swagger/
    - https://swagger.io/docs/specification/about/
    - https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md  

## Running the project

    $ npm start
