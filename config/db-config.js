var mysql = require("mysql");
var config = require('config')
var pool = mysql.createPool({
    connectionLimit: 10,
    connectTimeout: 3600000,
    acquireTimeout: 3600000,
    timeout: 3600000,
    host: config.get('SQL.HOST'),
    user: config.get('SQL.USER'),
    port: config.get('SQL.PORT'),
    database: config.get('SQL.DATABASE'),
    password:config.get('SQL.PASSWORD')
});
//console.log(pool)
module.exports = pool;