var config = require('config')
const mongoose = require('mongoose');
// Connecting to the database
module.exports.db_connect = mongo_connect
module.exports.mongoose = mongoose
function mongo_connect() {
    return mongoose.connect( config.get('MONGO.URL'), {
        useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true
    }).then(res => {
        console.log("Successfully connected to the database");  
        
         
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now...', err);

        
       
    });
}

