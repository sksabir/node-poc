const jwt = require('jsonwebtoken');
const jwtSecret = 'cnsecret007s'

module.exports = {
    signToken: async (data) => {
        return new Promise(async (resolve, reject) => {
            let token =jwt.sign({ data: data }, jwtSecret, { expiresIn: '7d' });
            resolve(token);
        })
    },
    verifyToken: async (token) => {
        return new Promise(async (resolve, reject) => {
            try {
                var decoded = await jwt.verify(token, jwtSecret);
                resolve(decoded);
            } catch (err) {
                console.log(err);
                reject(err);
            }
        })
    }
}