/* IMPORTING ALL THE FILES AND MODULES */
const express = require('express');
const helmet = require('helmet')
const config = require('config')
const errorHandler = require('./middleware/errorHandler')
var bodyParser = require('body-parser');
const jwt = require('./middleware/auth');
const db = require('./models');
const mongo_connect = require('./config/mongo-db-config')
const app = express();

/* BODY PARSING MIDDLEWARE */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

/* CHECKING THE DATABASE TYPE FROM ENV FILES AND CONNECTING TO THE DATABASE ACCORDINGLY */
if(config.get('DATABASE_TYPE')=='mongo')
{
     mongo_connect.db_connect()
}
else
{
     db.sequelize.sync({ force: false }).catch((err)=>{console.log(err)});
}
/* Helmet is a  Connect-style middleware that helps to secure HTTP headers returned by your Express app */
app.use(helmet());

//custom middleware example
 app.use(jwt())
 app.use(errorHandler);
require('./routes/router')(app);
const port = process.env.PORT || 3000;
require('./shared/swagger-connect')(app);
app.listen(port,()=>{
     console.log(`app listening on ${port} .....`)
     console.log(`The NODE_ENV is : ${process.env.NODE_ENV}`)
})
module.exports = app
