const mongo_db = require('../../config/mongo-db-config')
const mongoose = mongo_db.mongoose

const Image = mongoose.model(
  "Image",
  new mongoose.Schema({
    path: String,
    url: String,
    caption: String,
    createdAt: Date
  })
);

module.exports = Image;