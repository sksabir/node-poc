const mongo_db = require('../../config/mongo-db-config')
const mongoose = mongo_db.mongoose// var config = require('config')
const products = require("./product-model");

// mongoose.connect( config.get('mongo.url'), {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
//   .then(res => {console.log("Successfully connected to the database");})
//   .catch(err => {console.log('Could not connect to the database. Exiting now...', err);});
const UserSchema = mongoose.Schema({
        name: String,
        email:{
          type:String,
          unique: true
        },
        mobile:String,
        isDeleted:{
          type:String,
          enum:['0','1'],
          default:'0'
        },
        password:String
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);