const mongo_db = require('../../config/mongo-db-config')
const mongoose = mongo_db.mongoose

const Comment = mongoose.model(
  "Comment",
  new mongoose.Schema({
    username: String,
    text: String,
    createdAt: Date
  })
);

module.exports = Comment;