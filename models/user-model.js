const Joi = require('joi');
module.exports.user_table = (sequelize, type) => {
    return sequelize.define('user', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: type.STRING,
        email:{
          type:type.STRING,
          unique: true
        },
        mobile:type.STRING,
        isDeleted:{
          type:type.ENUM('0','1'),
          defaultValue:'0'
        },
        image:type.STRING,
        password:type.STRING,
        createdAt:type.DATE,
        updatedAt:type.DATE
    })
}


function validateUser(user,message=null){
  var schema;
  if(message=='all' || message ==null){
         schema = Joi.object({
          name: Joi.string().required(),
          email:Joi.required(),
          mobile:Joi.number().required(),
          password:Joi.required().optional()
      });
  
  }
  else if(message == 'id'){
        schema = Joi.object({
          id:Joi.required(),
          name: Joi.string().optional(),
          email:Joi.optional(),
          mobile:Joi.number().optional(),
          password:Joi.required().optional()
        })
  }
  const  { error, value } = schema.validate(user);
  if(error) {
      console.log(`The error is : ${error.message}`)
      return error.message 
  }
  else{
      return true
  }

}
module.exports.validateUser = validateUser
