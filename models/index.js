const config = require('config')

if(config.get('DATABASE_TYPE')=='sequelize'){
  const Sequelize = require("sequelize");
  const sequelize = new Sequelize(config.get('SQL.DATABASE'),config.get('SQL.USER'), config.get('SQL.PASSWORD'), {
    host: config.get('SQL.HOST'),
    dialect: "mysql",
    operatorsAliases: 0,
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  });
  
  const db = {};
  
  db.Sequelize = Sequelize;
  db.sequelize = sequelize;
  db.user1 = require("./user-model");
  db.product1 = require("./product-model");
  db.org1 = require("./organization-model")
  db.User = db.user1.user_table(sequelize, Sequelize)
  db.Product = db.product1.product_table(sequelize, Sequelize)
  db.Org = db.org1.org_table(sequelize,Sequelize)
  db.Product.belongsTo(db.User, { foreignKey: 'user_id' });
  db.User.hasMany(db.Product, { foreignKey: 'user_id' });
  db.Org.belongsTo(db.User, {  foreignKey : 'user_id'});
  
  module.exports = db;
}
else
{
  
  //finalResponses(res,data,'failure')
}
