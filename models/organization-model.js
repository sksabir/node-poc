const Joi = require('joi');


module.exports.org_table = (sequelize, type) => {
    return sequelize.define('organizations', {
            id:{
                type: type.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
            user_id:type.INTEGER,
            org_name:type.STRING,
            createdAt:type.DATE,
            updatedAt:type.DATE
    })
}