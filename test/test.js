process.env.NODE_ENV = 'test';


let User = require('../models/user-model');

let chai = require('chai');
let chaiHttp = require('chai-http');
const app = require('../index');
let should = chai.should();


chai.use(chaiHttp);
var expect = chai.expect;
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0NCwibmFtZSI6InVwZGF0ZWQxIG5hbWUiLCJlbWFpbCI6InN1cmJoaUBnbWFpbC5jb20iLCJtb2JpbGUiOiI0MjU0MjU1NDMyNSIsImlzRGVsZXRlZCI6IjEiLCJpbWFnZSI6bnVsbH0sImlhdCI6MTYyNTU2NzQwNSwiZXhwIjoxNjI2MTcyMjA1fQ.NxOrEfombNbq3ptUCrkA3oZSStkrhGFb3oi_BaoVgnE'
describe('Users', () => {
  /*
  * Test the /POST route
  */
  describe('/POST /api/create_user', () => {
   
    it('it should return 404 status', (done) => {    
      chai.request(app)
      .post('/api/create_users')
      .send({})
      .set({ Authorization: `Bearer ${token}` })
      .end(function (err, res) {
      expect(err).to.be.null;
      expect(res).to.have.status(404);
      
      done();
      });
  });
   //invalid token response
   it('it should return 401 status', (done) => {    
    chai.request(app)
    .post('/api/create-users')
    .send({})
    .end(function (err, res) {
    expect(err).to.be.null;
    expect(res).to.have.status(401);
    
    done();
    });
});
    //bad request 
    it('it should return 400 status', (done) => {    
    chai.request(app)
    .post('/api/create-user')
    .send({})
    .end(function (err, res) {
    expect(err).to.be.null;
    expect(res).to.have.status(400);
    //expect(res).to.have.lengthOf(2)
    
    done();
    });
});
      it('it should  create user', (done) => {
          let user = {
              "name":"shruti harsh",
              "email":"shrutiharsh12541@gmail.com",
              "mobile":42542554325,
              "password":"42354245"
          }
         
   chai.request(app)
  .post('/api/create-user')
  .send(user)
  .end(function (err, res) {
     expect(err).to.be.null;
     expect(res).to.have.status(200);
     console.log(res.body)
     res.body.should.be.a('object');
     res.body.data.should.be.a('object');
    //  res.body.length.should.be.eql(0);
     done();
  });

  });
})});

//update readme 